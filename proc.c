#include "types.h"
#include "defs.h"
#include "param.h"
#include "memlayout.h"
#include "mmu.h"
#include "x86.h"
#include "proc.h"
#include "uproc.h"
#include "spinlock.h"

#define HEAD 0 // for runnable lists
#define TAIL 1 // for runnable lists

struct prioQ {
  struct proc * head;
  struct proc * tail;
};

struct StateLists {
#ifdef CS333_P3P4
  struct prioQ runnable[MAXPRIO + 1]; // extra Q to simplify promotion scheme. See adjustUp()
#else
  struct proc* runnable;
#endif
  struct proc* unused;
  struct proc* sleep;
  struct proc* zombie;
  struct proc* running;
  struct proc* embryo;
};

struct {
  struct spinlock lock;
  struct proc proc[NPROC];
  struct StateLists pLists;
  uint   PromoteAtTime;
} ptable;

static struct proc *initproc;

int nextpid = 1;
extern void forkret(void);
extern void trapret(void);

static void wakeup1(void *chan);

void
pinit(void)
{
  initlock(&ptable.lock, "ptable");
  ptable.PromoteAtTime = TICKS_TO_PROMOTE; // for global MLFQ promotion
}

//PAGEBREAK: 32
// Look in the process table for an UNUSED proc.
// If found, change state to EMBRYO and initialize
// state required to run in the kernel.
// Otherwise return 0.
static struct proc*
allocproc(void)
{
  struct proc *p;
  char *sp;

  acquire(&ptable.lock);
#ifndef CS333_P3P4
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
    if(p->state == UNUSED)
      goto found;
#else
  if ((p = popStateList(&ptable.pLists.unused, UNUSED)) != 0)
    goto found;
#endif
  release(&ptable.lock);
  return 0;

found:
  p->state = EMBRYO;
#ifdef CS333_P3P4
  addToStateListFront(&ptable.pLists.embryo, p, EMBRYO);
#endif
  p->pid = nextpid++;
  release(&ptable.lock);

  // Allocate kernel stack.
  if((p->kstack = kalloc()) == 0){
    acquire(&ptable.lock);
#ifndef CS333_P3P4
    p->state = UNUSED;
#else
    removeFromStateList(&ptable.pLists.embryo, p, EMBRYO);
    p->state = UNUSED;
    addToStateListFront(&ptable.pLists.unused, p, UNUSED);
#endif
    release(&ptable.lock);
    return 0;
  }
  sp = p->kstack + KSTACKSIZE;
  
  // Leave room for trap frame.
  sp -= sizeof *p->tf;
  p->tf = (struct trapframe*)sp;
  
  // Set up new context to start executing at forkret,
  // which returns to trapret.
  sp -= 4;
  *(uint*)sp = (uint)trapret;

  sp -= sizeof *p->context;
  p->context = (struct context*)sp;
  memset(p->context, 0, sizeof *p->context);
  p->context->eip = (uint)forkret;
  p->start_ticks = ticks;
  p->cpu_ticks_total = 0;
  p->cpu_ticks_in = 0;
  p->budget = DEFAULT_BUDGET;
  p->prio   = 0;

  return p;
}

//PAGEBREAK: 32
// Set up first user process.
void
userinit(void)
{
  struct proc *p;
  extern char _binary_initcode_start[], _binary_initcode_size[];
  
#ifdef CS333_P3P4
  acquire(&ptable.lock);
  struct prioQ* Q = ptable.pLists.runnable;
  for (int i = 0; i <= MAXPRIO; ++i){
    Q[i].head = 0;
    Q[i].tail = 0;
  }
  ptable.pLists.sleep    = 0;
  ptable.pLists.zombie   = 0;
  ptable.pLists.running  = 0;
  ptable.pLists.embryo   = 0;
  ptable.pLists.unused   = ptable.proc;
  for (int i = 0; i < NPROC - 1; ++i){
    ptable.proc[i].state = UNUSED;
    ptable.proc[i].next  = &ptable.proc[i + 1];
  }
  ptable.proc[NPROC - 1].state = UNUSED;
  ptable.proc[NPROC - 1].next = 0;
  release(&ptable.lock);
#endif

  p = allocproc();
  acquire(&ptable.lock);
  initproc = p;
  if((p->pgdir = setupkvm()) == 0)
    panic("userinit: out of memory?");
  inituvm(p->pgdir, _binary_initcode_start, (int)_binary_initcode_size);
  p->sz = PGSIZE;
  memset(p->tf, 0, sizeof(*p->tf));
  p->tf->cs = (SEG_UCODE << 3) | DPL_USER;
  p->tf->ds = (SEG_UDATA << 3) | DPL_USER;
  p->tf->es = p->tf->ds;
  p->tf->ss = p->tf->ds;
  p->tf->eflags = FL_IF;
  p->tf->esp = PGSIZE;
  p->tf->eip = 0;  // beginning of initcode.S

  safestrcpy(p->name, "initcode", sizeof(p->name));
  p->cwd = namei("/");

  p->uid = DEFAULT_UID;
  p->gid = DEFAULT_GID;
  p->parent = p;
#ifndef CS333_P3P4
  p->state = RUNNABLE;
#else
  removeFromStateList(&ptable.pLists.embryo, p, EMBRYO);
  p->state = RUNNABLE;
  enqueueRunnable(p);
#endif
  release(&ptable.lock);
}

// Grow current process's memory by n bytes.
// Return 0 on success, -1 on failure.
int
growproc(int n)
{
  uint sz;
  
  sz = proc->sz;
  if(n > 0){
    if((sz = allocuvm(proc->pgdir, sz, sz + n)) == 0)
      return -1;
  } else if(n < 0){
    if((sz = deallocuvm(proc->pgdir, sz, sz + n)) == 0)
      return -1;
  }
  proc->sz = sz;
  switchuvm(proc);
  return 0;
}

// Create a new process copying p as the parent.
// Sets up stack to return as if from system call.
// Caller must set state of returned proc to RUNNABLE.
int
fork(void)
{
  int i, pid;
  struct proc *np;

  // Allocate process.
  if((np = allocproc()) == 0)
    return -1;

  // Copy process state from p.
  if((np->pgdir = copyuvm(proc->pgdir, proc->sz)) == 0){
    kfree(np->kstack);
    np->kstack = 0;
    acquire(&ptable.lock);
    // move back to unused list
#ifndef CS333_P3P4
    np->state = UNUSED;
#else
    removeFromStateList(&ptable.pLists.embryo, np, EMBRYO);
    np->state = UNUSED;
    addToStateListFront(&ptable.pLists.unused, np, UNUSED);
#endif
    release(&ptable.lock);
    return -1;
  }
  np->sz = proc->sz;
  np->parent = proc;
  *np->tf    = *proc->tf;
  np->gid    = proc->gid; 
  np->uid    = proc->uid;

  // Clear %eax so that fork returns 0 in the child.
  np->tf->eax = 0;

  for(i = 0; i < NOFILE; i++)
    if(proc->ofile[i])
      np->ofile[i] = filedup(proc->ofile[i]);
  np->cwd = idup(proc->cwd);

  safestrcpy(np->name, proc->name, sizeof(proc->name));
 
  pid = np->pid;

  // lock to force the compiler to emit the np->state write last.
  acquire(&ptable.lock);
#ifndef CS333_P3P4
  np->state = RUNNABLE;
#else
  removeFromStateList(&ptable.pLists.embryo, np, EMBRYO);
  np->state = RUNNABLE;
  enqueueRunnable(np);
#endif
  release(&ptable.lock);
  
  return pid;
}

// Exit the current process.  Does not return.
// An exited process remains in the zombie state
// until its parent calls wait() to find out it exited.
#ifndef CS333_P3P4
void
exit(void)
{
  struct proc *p;
  int fd;

  if(proc == initproc)
    panic("init exiting");

  // Close all open files.
  for(fd = 0; fd < NOFILE; fd++){
    if(proc->ofile[fd]){
      fileclose(proc->ofile[fd]);
      proc->ofile[fd] = 0;
    }
  }

  begin_op();
  iput(proc->cwd);
  end_op();
  proc->cwd = 0;

  acquire(&ptable.lock);

  // Parent might be sleeping in wait().
  wakeup1(proc->parent);

  // Pass abandoned children to init.
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
    if(p->parent == proc){
      p->parent = initproc;
      if(p->state == ZOMBIE)
        wakeup1(initproc);
    }
  }

  // Jump into the scheduler, never to return.
  proc->state = ZOMBIE;
  sched();
  panic("zombie exit");
}
#else
void
exit(void)
{
  struct proc *p;
  int fd;

  if(proc == initproc)
    panic("init exiting");

  // Close all open files.
  for(fd = 0; fd < NOFILE; fd++){
    if(proc->ofile[fd]){
      fileclose(proc->ofile[fd]);
      proc->ofile[fd] = 0;
    }
  }

  begin_op();
  iput(proc->cwd);
  end_op();
  proc->cwd = 0;

  acquire(&ptable.lock);

  // Parent might be sleeping in wait().
  wakeup1(proc->parent);

  // Pass abandoned children to init.
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
    if(p->parent == proc){
      p->parent = initproc;
      if(p->state == ZOMBIE)
        wakeup1(initproc);
    }
  }

  // Jump into the scheduler, never to return.
  removeFromStateList(&ptable.pLists.running, proc, RUNNING);
  proc->state = ZOMBIE;
  addToStateListFront(&ptable.pLists.zombie, proc, ZOMBIE);
  sched();
  panic("zombie exit");
}
#endif

// Wait for a child process to exit and return its pid.
// Return -1 if this process has no children.
#ifndef CS333_P3P4
int
wait(void)
{
  struct proc *p;
  int havekids, pid;

  acquire(&ptable.lock);
  for(;;){
    // Scan through table looking for zombie children.
    havekids = 0;
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
      if(p->parent != proc)
        continue;
      havekids = 1;
      if(p->state == ZOMBIE){
        // Found one.
        pid = p->pid;
        kfree(p->kstack);
        p->kstack = 0;
        freevm(p->pgdir);
        p->state = UNUSED;
        p->pid = 0;
        p->parent = 0;
        p->name[0] = 0;
        p->killed = 0;
        release(&ptable.lock);
        return pid;
      }
    }

    // No point waiting if we don't have any children.
    if(!havekids || proc->killed){
      release(&ptable.lock);
      return -1;
    }

    // Wait for children to exit.  (See wakeup1 call in proc_exit.)
    sleep(proc, &ptable.lock);  //DOC: wait-sleep
  }
}
#else
int
wait(void)
{
  struct proc *p;
  int havekids, pid;

  acquire(&ptable.lock);
  for(;;){
    // Scan through table looking for zombie children.
    havekids = 0;
    // RUNNABLE
    for(uint i = 0; i < MAXPRIO && !havekids; ++i){
      p = ptable.pLists.runnable[i].head;
      while(p && !havekids){
        if (p->parent == proc)
          havekids = 1;
        else
          p = p->next;
      }
    }
    // RUNNING
    if (!havekids){
      p = ptable.pLists.running;
      while (p && !havekids){
        if (p->parent == proc)
          havekids = 1;
        else
          p = p->next;
      }
    }
    // SLEEP
    if (!havekids){
      p = ptable.pLists.sleep;
      while (p && !havekids){
        if (p->parent == proc)
          havekids = 1;
        else
          p = p->next;
      }
    }
    // ZOMBIE
    p = ptable.pLists.zombie;
    while (p){
      if (p->parent == proc){
        havekids = 1;
        pid = p->pid;
        kfree(p->kstack);
        p->kstack = 0;
        freevm(p->pgdir);
        removeFromStateList(&ptable.pLists.zombie, p, ZOMBIE);
        p->state = UNUSED;
        addToStateListFront(&ptable.pLists.unused, p, UNUSED);
        p->pid = 0;
        p->parent = 0;
        p->name[0] = 0;
        p->killed = 0;
        release(&ptable.lock);
        return pid;
      }
      else
        p = p->next;
    }
    // No point waiting if we don't have any children.
    if (!havekids || proc->killed){
      release(&ptable.lock);
      return -1;
    }
    // Wait for children to exit.  (See wakeup1 call in proc_exit.)
    sleep(proc, &ptable.lock);  //DOC: wait-sleep
  }
}
#endif

//PAGEBREAK: 42
// Per-CPU process scheduler.
// Each CPU calls scheduler() after setting itself up.
// Scheduler never returns.  It loops, doing:
//  - choose a process to run
//  - swtch to start running that process
//  - eventually that process transfers control
//      via swtch back to the scheduler.
#ifndef CS333_P3P4
// original xv6 scheduler. Use if CS333_P3P4 NOT defined.
void
scheduler(void)
{
  struct proc *p;
  int idle;  // for checking if processor is idle

  for(;;){
    // Enable interrupts on this processor.
    sti();

    idle = 1;  // assume idle unless we schedule a process
    // Loop over process table looking for process to run.
    acquire(&ptable.lock);
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
      if(p->state != RUNNABLE)
        continue;

      // Switch to chosen process.  It is the process's job
      // to release ptable.lock and then reacquire it
      // before jumping back to us.
      idle = 0;  // not idle this timeslice
      proc = p;
      switchuvm(p);
      p->state = RUNNING;
      p->cpu_ticks_in = ticks; // track cpu use time for proc
      swtch(&cpu->scheduler, proc->context);
      switchkvm();

      // Process is done running for now.
      // It should have changed its p->state before coming back.
      proc = 0;
    }
    release(&ptable.lock);
    // if idle, wait for next interrupt
    if (idle) {
      sti();
      hlt();
    }
  }
}

#else
void
scheduler(void)
{
  struct proc *p;
  int idle;  // for checking if processor is idle

  for(;;){
    // Enable interrupts on this processor.
    sti();
    idle = 1;  // assume idle unless we schedule a process

    acquire(&ptable.lock);

    if (ticks > ptable.PromoteAtTime)
      adjustUp();
    
    // traverse MLFQ
    for (int i = 0; i < MAXPRIO; ++i){
      if ((p = dequeueRunnable(i)) != 0)
        break;
    }

    if(p){       // Switch to chosen process
      idle = 0;  // not idle this timeslice
      proc = p;
      switchuvm(p);
      p->state = RUNNING;
      addToStateListFront(&ptable.pLists.running, p, RUNNING); // move to RUNNING Statelist
      p->cpu_ticks_in = ticks; // track cpu use time for proc
      swtch(&cpu->scheduler, proc->context);
      switchkvm();

      // Process is done running for now.
      // It should have changed its p->state before coming back.
      proc = 0;
    }
    release(&ptable.lock);
    // if idle, wait for next interrupt
    if (idle) {
      sti();
      hlt();
    }
  }
}
#endif

// Enter scheduler.  Must hold only ptable.lock
// and have changed proc->state.
#ifndef CS333_P3P4
void
sched(void)
{
  int intena;

  if(!holding(&ptable.lock))
    panic("sched ptable.lock");
  if(cpu->ncli != 1)
    panic("sched locks");
  if(proc->state == RUNNING)
    panic("sched running");
  if(readeflags()&FL_IF)
    panic("sched interruptible");
  intena = cpu->intena;
  proc->cpu_ticks_total += ticks - proc->cpu_ticks_in; // update total cpu time
  swtch(&proc->context, cpu->scheduler);
  cpu->intena = intena;
}
#else
void
sched(void)
{
  int intena;

  if(!holding(&ptable.lock))
    panic("sched ptable.lock");
  if(cpu->ncli != 1)
    panic("sched locks");
  if(proc->state == RUNNING)
    panic("sched running");
  if(readeflags()&FL_IF)
    panic("sched interruptible");
  intena = cpu->intena;

  proc->cpu_ticks_total += (ticks - proc->cpu_ticks_in); // update total cpu time
  proc->budget          -= (ticks - proc->cpu_ticks_in); // decrement budget

  if (proc->budget <= 0){
    proc->budget = DEFAULT_BUDGET;
    if(proc->prio < (MAXPRIO-1)){ 
      if (proc->state == RUNNABLE){
        removeFromRunnable(proc); 
        proc->prio++;
        enqueueRunnable(proc);
      }
      else
        proc->prio++;
    }
  }

  swtch(&proc->context, cpu->scheduler);
  cpu->intena = intena;
}
#endif

// Give up the CPU for one scheduling round.
void
yield(void)
{
  acquire(&ptable.lock);  //DOC: yieldlock
#ifndef CS333_P3P4
  proc->state = RUNNABLE;
#else
  removeFromStateList(&ptable.pLists.running, proc, RUNNING);
  proc->state = RUNNABLE;
  enqueueRunnable(proc);
#endif
  sched();
  release(&ptable.lock);
}

// A fork child's very first scheduling by scheduler()
// will swtch here.  "Return" to user space.
void
forkret(void)
{
  static int first = 1;
  // Still holding ptable.lock from scheduler.
  release(&ptable.lock);

  if (first) {
    // Some initialization functions must be run in the context
    // of a regular process (e.g., they call sleep), and thus cannot 
    // be run from main().
    first = 0;
    iinit(ROOTDEV);
    initlog(ROOTDEV);
  }
  
  // Return to "caller", actually trapret (see allocproc).
}

// Atomically release lock and sleep on chan.
// Reacquires lock when awakened.
// 2016/12/28: ticklock removed from xv6. sleep() changed to
// accept a NULL lock to accommodate.
void
sleep(void *chan, struct spinlock *lk)
{
  if(proc == 0)
    panic("sleep");

  // Must acquire ptable.lock in order to
  // change p->state and then call sched.
  // Once we hold ptable.lock, we can be
  // guaranteed that we won't miss any wakeup
  // (wakeup runs with ptable.lock locked),
  // so it's okay to release lk.
  if(lk != &ptable.lock){
    acquire(&ptable.lock);
    if (lk) release(lk);
  }

  // Go to sleep.
  proc->chan = chan;
#ifndef CS333_P3P4
  proc->state = SLEEPING;
#else
  removeFromStateList(&ptable.pLists.running, proc, RUNNING);
  proc->state = SLEEPING;
  addToStateListFront(&ptable.pLists.sleep, proc, SLEEPING);
#endif
  proc->cpu_ticks_total += ticks - proc->cpu_ticks_in; // update total cpu time
  sched();

  // Tidy up.
  proc->chan = 0;

  // Reacquire original lock.
  if(lk != &ptable.lock){ 
    release(&ptable.lock);
    if (lk) acquire(lk);
  }
}

//PAGEBREAK!
#ifndef CS333_P3P4
// Wake up all processes sleeping on chan.
// The ptable lock must be held.
static void
wakeup1(void *chan)
{
  struct proc *p;

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
    if(p->state == SLEEPING && p->chan == chan)
      p->state = RUNNABLE;
}
#else
static void
wakeup1(void *chan)
{
  struct proc *p = ptable.pLists.sleep;
  struct proc *next;

  while(p){
    next = p->next;
    if(p->chan == chan){
      removeFromStateList(&ptable.pLists.sleep, p, SLEEPING);
      p->state = RUNNABLE;
      enqueueRunnable(p);
    }
    p = next;
  }
}
#endif

// Wake up all processes sleeping on chan.
void
wakeup(void *chan)
{
  acquire(&ptable.lock);
  wakeup1(chan);
  release(&ptable.lock);
}

// Kill the process with the given pid.
// Process won't exit until it returns
// to user space (see trap in trap.c).
#ifndef CS333_P3P4
int
kill(int pid)
{
  struct proc *p;

  acquire(&ptable.lock);
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
    if(p->pid == pid){
      p->killed = 1;
      // Wake process from sleep if necessary.
      if(p->state == SLEEPING)
        p->state = RUNNABLE;
      release(&ptable.lock);
      return 0;
    }
  }
  release(&ptable.lock);
  return -1;
}
#else
int
kill(int pid)
{
  int    found = 0;
  struct proc *p;

  acquire(&ptable.lock);
  // RUNNABLE
  for(uint i = 0; i < MAXPRIO && !found; ++i){
    p = ptable.pLists.runnable[i].head;
    while(p && !found){
      if (p->pid == pid)
        found = 1;
      else
        p = p->next;
    }
  }
  // RUNNING
  if (!found){
    p = ptable.pLists.running;
    while (p && !found){
      if (p->pid == pid)
        found = 1;
      else
        p = p->next;
    }
  }
  // SLEEP
  if (!found){
    p = ptable.pLists.sleep;
    while (p && !found){
      if (p->pid == pid){
        removeFromStateList(&ptable.pLists.sleep, p, SLEEPING);
        p->state = RUNNABLE;
        p->prio  = 0;
        enqueueRunnable(p);
        found = 1;
      }
      else
        p = p->next;
    }
  }
  if (found){
    p->prio   = 0;
    p->killed = 1;
    release(&ptable.lock);
    return 0;
  }
  release(&ptable.lock);
  return -1;
}
#endif

static char *states[] = {
  [UNUSED]    "unused",
  [EMBRYO]    "embryo",
  [SLEEPING]  "sleep ",
  [RUNNABLE]  "runble",
  [RUNNING]   "run   ",
  [ZOMBIE]    "zombie"
};

//PAGEBREAK: 36
// Print a process listing to console.  For debugging.
// Runs when user types ^P on console.
// No lock to avoid wedging a stuck machine further.
void
procdump(void)
{
  int i, elapsed;
  struct proc *p;
  char *state;
  uint pc[10];
  
  // table header
  cprintf("\nPID \tName \tUID \tGID \tPPID \tPRIO \tElapsed\tCPU \tState \tSize \t PCs\n");

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
    if(p->state == UNUSED || p->state == EMBRYO)
      continue;
    if(p->state >= 0 && p->state < NELEM(states) && states[p->state])
      state = states[p->state];
    else
      state = "???";

    elapsed = ticks - p->start_ticks;
    cprintf("%d \t%s \t%d \t%d \t%d \t%d \t%d.%d%d \t%d.%d%d \t%s \t%d\t", 
        p->pid, 
        p->name,
        p->uid,
        p->gid,
        p->parent->pid,
        p->prio,
        elapsed / 100,
        (elapsed % 100) / 10,
        elapsed % 10,
        p->cpu_ticks_total / 100,
        (p->cpu_ticks_total % 100) / 10,
        p->cpu_ticks_total % 10,
        state,
        p->sz);

    // PCs
    if(p->state == SLEEPING){
      getcallerpcs((uint*)p->context->ebp+2, pc);
      for(i=0; i<10 && pc[i] != 0; i++)
        cprintf(" %p", pc[i]);
    }
    cprintf("\n");
  }
}

void
statelistdump(enum procstate state)
{
  acquire(&ptable.lock);
  struct proc* i;
  ushort count = 0;
  switch (state)
  {
    case UNUSED:
      i = ptable.pLists.unused;
      while (i){ // traverse list, handles empty
        i = i->next;
        ++count;
      }
      cprintf("Unused list size: %d processes\n", count);
      break;
    case RUNNABLE:
      cprintf("Runnable List Processes:\n", count);
      for (int idx = 0; idx < MAXPRIO; ++idx){
        i = ptable.pLists.runnable[idx].head;
        cprintf("Q%d:", idx);
        if (!i)
          cprintf(" empty");
        while (i != 0){
          cprintf(" (%d, %d) ->", i->pid, i->budget);
          i = i->next;
        }
        cprintf("\n");
      }
      break;
    case SLEEPING:
      cprintf("Sleep List Processes:\n", count);
      i = ptable.pLists.sleep;
      while (i){
        cprintf("%d -> ", i->pid);
        i = i->next;
      }
      cprintf("\n");
      break;
    case ZOMBIE:
      cprintf("Zombie List Processes:\n", count);
      i = ptable.pLists.zombie;
      while (i){
        cprintf("(%d, %d) -> ", i->pid, i->parent->pid);
        i = i->next;
      }
      cprintf("\n");
      break;
    default:
      break;
  }
  release(&ptable.lock);
}

// subroutine of command 'ps'
// populates an array of uprocs based on ptable
int
getuprocs(const uint MAX, struct uproc* u)
{
  int count = 0;
  struct proc* p;

  acquire(&ptable.lock);
  for(p = ptable.proc; p <= &ptable.proc[NPROC - 1] && count < MAX; ++p)
  {
    if (p->state == UNUSED || p->state == EMBRYO) // skip inactive processes
      continue;

    u->pid = p->pid;
    u->uid = p->uid;
    u->gid = p->gid;
    u->ppid = p->parent->pid;
    u->prio = p->prio;
    u->elapsed_ticks = ticks - p->start_ticks;
    u->cpu_ticks_total = p->cpu_ticks_total;
    safestrcpy(u->state, states[p->state], sizeof(u->state));
    u->sz = p->sz;
    safestrcpy(u->name, p->name, sizeof(u->name));
    ++u;
    ++count;
  }
  release(&ptable.lock);
  return count;
}

/* ----- STATE LIST MANAGEMENT FUNCTIONS ----- */
// Must have ptable.lock to call any of these functions!

// anonymous procs can be popped from front. Should allow return of NULL ptr
struct proc*
popStateList(struct proc** head, enum procstate state)
{
  struct proc* ret = *head;
  if (ret){
    if (ret->state != state){
      cprintf("Trying to remove proc with state %s from %s list!\n", states[(*head)->state], states[state]);
      panic("StateList err");
    }
    *head = (*head)->next;
    ret->next = 0;
  }
  return ret;
}

// remove a proc from a state list. panics if state is not correct
int
removeFromStateList(struct proc** curr, struct proc* p, enum procstate state)
{
  if (p->state != state){
      cprintf("Trying to remove proc with state %s from %s list!\n", states[p->state], states[state]);
      panic("StateList err");
  }

  while (*curr && *curr != p)
    curr = &(*curr)->next;

  if (!*curr){
    cprintf("Could not find proc %s on %s list!\n", p->name, states[state]);
    panic("StateList err");
  }

  *curr = p->next; // reconnect
  p->next = 0;
  return 1;
}

// pushes a proc onto an unordered state list. panics if the proc is not in correct state
void
addToStateListFront(struct proc** head, struct proc* p, enum procstate state)
{
  if (p->state != state){
    cprintf("Trying to add proc in state %s to %s list!\n", states[p->state], states[state]);
    panic("StateList err");
  }
  p->next = *head;
  *head = p; // move head to new proc
}

void
enqueueRunnable(struct proc* p)
{
  if (p->state != RUNNABLE){
    cprintf("Trying to add proc in state %s to %s list!\n", states[p->state], states[RUNNABLE]);
    panic("StateList err");
  }
  if (p->prio < 0 || p->prio >= MAXPRIO)
    panic("process has invalid priority!");

  struct prioQ *Q = &ptable.pLists.runnable[p->prio];

  if (!Q->head)
    Q->head = p;
  else
    Q->tail->next = p;
  Q->tail = p;
  p->next = 0;
}

struct proc *
dequeueRunnable(int prio)
{
  struct prioQ *Q   = &ptable.pLists.runnable[prio];
  struct proc  *ret = Q->head;

  if (ret){ // not empty
    if (ret->state != RUNNABLE){
      cprintf("Trying to remove proc in state %s from %s list!\n", states[ret->state], states[RUNNABLE]);
      panic("StateList err");
    }
    if (Q->head == Q->tail) // last item in list
      Q->head = Q->tail = 0;
    else
      Q->head = ret->next;
    ret->next = 0;
  }
  return ret;
}

void
removeFromRunnable(struct proc *p)
{
  struct prioQ *Q    = &ptable.pLists.runnable[p->prio];
  struct proc  *curr = Q->head;
  struct proc  *prev = Q->head;

  while (curr && curr != p){
    prev = curr;
    curr = curr->next;
  }
  if (!curr)
    panic("proc not on Runnable list!");

  // Special cases
  if (Q->head == Q->tail)
    Q->tail = Q->head = 0;
  else if (curr == Q->tail){ // end of list
    Q->tail    = prev;
    prev->next = 0;
  }
  else if (curr == Q->head)
    Q->head = curr->next;
  else
    prev->next = curr->next;
  p->next = 0;
}

// decrements priority of every process if prio > 0 and resets al budgets
void
adjustUp()
{
  // RUNNABLE
  struct prioQ *Q    = ptable.pLists.runnable;
  struct proc  *curr = Q[0].head;
  while(curr){
    curr->budget = DEFAULT_BUDGET;
    curr = curr->next;
  }

  // Q0 special cases
  if (Q[0].head)
    Q[0].tail->next = Q[1].head; // join Q1 to end of Q0
  else
    Q[0].head = Q[1].head;
  if (Q[1].tail)
    Q[0].tail = Q[1].tail;

  // Q1 -- QMAX
  for (int i = 1; i < MAXPRIO; ++i){ // adjust prios and budgets
    curr = Q[i].head;
    while(curr){
      curr->budget = DEFAULT_BUDGET;
      curr->prio--;
      curr = curr->next;
    }
    Q[i].head = Q[i+1].head; // shift head and tail pointers to next queue
    Q[i].tail = Q[i+1].tail;
  }

  curr = ptable.pLists.sleep;
  while (curr){
    curr->budget = DEFAULT_BUDGET;
    if(curr->prio > 0)
      curr->prio--;
    curr = curr->next;
  }
  curr = ptable.pLists.running;
  while (curr){
    curr->budget = DEFAULT_BUDGET;
    if(curr->prio > 0)
      curr->prio--;
    curr = curr->next;
  }
  ptable.PromoteAtTime = ticks + TICKS_TO_PROMOTE;
}

// sets priority of a particular active process
int
setprio(uint pid, int prio)
{
  if (prio > MAXPRIO || prio < 0)
    return -1;

  acquire(&ptable.lock);

  struct proc *p = 0;
  for (uint i = 0; i < MAXPRIO; ++i){
    p = ptable.pLists.runnable[i].head;
    while (p && p->pid != pid)
      p = p->next;
    if (p)
      break;
  }

  if (!p){
    p = ptable.pLists.sleep;
    while (p && p->pid != pid)
      p = p->next;
  }
  
  if (!p){
    p = ptable.pLists.running;
    while (p && p->pid != pid)
      p = p->next;
  }

  if (p){
    if (p->state == RUNNABLE && p->prio != prio) { // found
      removeFromRunnable(p);
      p->prio = prio;
      enqueueRunnable(p);
    }
    p->prio   = prio;
    p->budget = DEFAULT_BUDGET;
  }

  release(&ptable.lock);
  return (int) p; // 0 if not found
}
/* ----- END STATE LIST MANAGEMENT FUNCTIONS ----- */
