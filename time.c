// time.c
// This program takes a list of arguments, the first of which is another
// user command. The amount of CPU time taken to execute the command is 
// reported.

#include "types.h"
#include "user.h"

int
main(int argc, char* argv[])
{
  uint ticks = 0;
  char* proc_name = "";

  if (argc > 1)
  {
    proc_name = argv[1];
    int pid;

    ticks = uptime(); // initialize to global ticks right before fork
    pid = fork();

    if(pid == 0) // child
    {
      exec(argv[1], &argv[1]); // should not return
      printf(2, "could not find command %s...\n", argv[1]);
      exit();
    }
    else if (pid > 0) // parent
    {
      wait();
      ticks = uptime() - ticks;
    }
    else
    {
      // fork error
      printf(2, "an error occurred while running %s\n", proc_name);
      exit();
    }
  }
  else
  {
    // no argument given
  }

  printf(1, "%s finished in %d.%d%d s\n",
            proc_name,
            ticks / 100,
            (ticks % 100) / 10,
            ticks % 10);

  exit();
}
