#include "types.h"
#include "user.h"

int
testuidgid(void)
{
  uint nval, uid, gid, ppid;

  uid = getuid();
  nval = 100;
  printf(1, "Current UID is: %d\n", uid);
  printf(1, "Setting UID to %d\n", nval);
  if (setuid(nval) < 0)
    printf(2, "Error. Invalid UID: %d\n", nval);

  uid = getuid();
  printf(1, "Current UID is: %d\n", uid);

  sleep(5000);  // for testing ^p and ps

  gid = getgid();
  nval = 200;
  printf(1, "Current GID is: %d\n", gid);
  printf(1, "Setting GID to %d\n", nval);
  if (setgid(nval) < 0)
    printf(2, "Error. Invalid GID: %d\n", nval);

  setgid(nval);
  gid = getgid();
  printf(1, "Current GID is: %d\n", gid);

  sleep(5000);  // for testing ^p and ps

  ppid = getppid();
  printf(1, "My parent process is: %d\n", ppid);

  // tests for invalid values

  nval = 32800;   // 32767 is max value
  printf(1, "Setting UID to %d. This test should FAIL\n", nval);
  if (setuid(nval) < 0)
    printf(1, "SUCCESS! The setuid sytem call indicated failure\n");
  else
    printf(2, "FAILURE! The setuid system call indicates success\n");


  nval = 32800;   // 32767 is max value
  printf(1, "Setting GID to %d. This test should FAIL\n", nval);
  if (setgid(nval) < 0)
    printf(1, "SUCCESS! The setgid sytem call indicated failure\n");
  else
    printf(2, "FAILURE! The setgid system call indicates success\n");

  printf(1, "Done!\n");

  return 0;
}

int
main() {
  testuidgid();

  exit();
}
