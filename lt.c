// lt.c
// This user program creates collections of processes ideal for monitoring state list behavior
// The new control sequences are used to view information while these routines run.
#include "types.h"
#include "param.h"
#include "user.h"

#define PRIO_COUNT  10
#define CHILD_COUNT 10

void
zombieFree()
{
  uint pids[NPROC]; // NPROC will always be larger than available forks b/c of init, sh, and zombieFree procs
  int idx = 0;
  int pid = 1;
  
  printf(1, "forking until failure...\n");
  while (pid > 0){ // fork max num procs
    pid = fork();
    pids[idx++] = pid;
    if(pid == 0){  // child
      sleep(1000000);
      exit();
    }
  }
  printf(1, "%d children forked\n", idx - 1);
  sleep(200);
  for (int i = 0; i < idx - 1; ++i)
  {
    printf(1, "killing %d\n", pids[i]);
    kill(pids[i]);
    sleep(100); // wait a bit so can see zombie list
    while(wait() == -1);
  }
}

// several processes are forked that use their entire timeslice. This allows
// observing of the Round-Robin behavior of RUNNABLE
void
testRunnable()
{
  uint N = 9;
  uint pids[N];

  printf(1, "forking %d children that will spin, using entire time slices...", N);
  for (int n = 0; n < N; ++n){ // creates N children
    pids[n] = fork(); 
    if (pids[n] == 0) // child
      for(;;); // spin to use entire time-slice. Best case to observe changing runnable list
  }
  printf(1, "done\n");
  sleep(3000);
  for (int i = 0; i < N; ++i){
    printf(1, "killing %d\n", pids[i]);
    kill(pids[i]);
    while(wait() == -1);
  }
}

// tests a process that transitions from sleep to running 
void
testSleeping()
{
  uint pid = fork();
  if (pid == 0) {// child 
    for(;;){
      sleep(10); // sleep
      for (int i = 0; i < 10000000; ++i); // do work
    }
  }
  else {
    printf(1, "creating child %d that will alternate work and sleep\n", pid);
    sleep(2000);
    printf(1, "killing %d\n", pid);
    kill(pid);
    while(wait() == -1);
  }
}

void
testsetpriority()
{
  unsigned long count = 0;
  uint pids[CHILD_COUNT];
  for (uint i = 0; i < CHILD_COUNT; ++i){
    pids[i] = fork();
    if (pids[i] == 0) // child
      while(1) {++count;}
  }
  for (uint i = 0; i < CHILD_COUNT; ++i){
    if (i < 5)
      setpriority(pids[i], 0);
    else
      setpriority(pids[i], MAXPRIO - 1);
    //printf(1, "%d: new prio %d\n", pids[i], j %(MAXPRIO));
  }
  sleep(2000);
  for (uint i = 0; i < CHILD_COUNT; ++i){
    printf(1, "killing %d\n", pids[i]);
    kill(pids[i]);
    while(wait() == -1);
  }
}

void
testsetpriorityNonRunnable()
{
  char * arg[] = {"no", "args"};
  int pid = fork();
  if (pid == 0){
    exec("sh", arg);
    printf(2, "exec failure\n");
  }
  else{
    sleep(200);
    printf(1, "changing pid %d to priority %d\n", pid, MAXPRIO - 2);
    setpriority(pid, MAXPRIO - 2);
    sleep(500);
    printf(1, "killing %d\n", pid);
    kill(pid);
    wait();
  }
}

int
main(int argc, char* argv[])
{
  if (argc < 2){
    printf(2, "usage:\nlt [p|P|r|s|z]\n");
    exit();
  }

  switch (argv[1][0])
  {
    case 'p':
      testsetpriority();
      break;
    case 'P':
      testsetpriorityNonRunnable();
      break;
    case 'r': case 'R':
      testRunnable();
      break;
    case 's': case 'S':
      testSleeping();
      break;
    case 'z': case 'Z':
      zombieFree();
      break;
    default:
      printf(2, "invalid argument!");
  }
  exit();
}
