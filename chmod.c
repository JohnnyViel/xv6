// chmod.c
// This program takes a 4-digit octal number to set the permissions
// of a file or directory. The first digit (0 or 1) specifies setuid
#include "types.h"
#include "user.h"

#ifdef CS333_P5
void errx(char * msg){
  printf(2, msg);
  exit();
}

int
main(int argc, char *argv[])
{
  char *c = argv[1];
  uint decmode = 0;

  if (argc != 3)            errx("usage: chmod expects 2 arguments\n");
  if (strlen(argv[1]) != 4) errx("usage: mode must be 4 digits long\n");
  if (argv[1][0] == '-')    errx("usage: mode must be a positive octal value between 0 - 1777\n");

  // check setuid bit
  if (*c != '0' && *c != '1')
    errx("setuid bit must be a 0 or 1\n");

  // get u-g-o values
  while (c < &argv[1][4]){
    if (*c < '0' || '7' < *c)
      errx("Invalid digit in mode. Must be a positive octal value between 0 - 1777\n");
    decmode = decmode * 8 + *c++ - '0';
  }

  if (chmod(argv[2], decmode))
    errx("chmod failed\n");
  exit();
}

#else
int main(){
  printf(2, "chomd not implemented without CS333_P5 flag defined in makefile\n");
  exit();
}
#endif
