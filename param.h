#define NPROC             64  // maximum number of processes
#define KSTACKSIZE      4096  // size of per-process kernel stack
#define NCPU               8  // maximum number of CPUs
#define NOFILE            16  // open files per process
#define NFILE            100  // open files per system
#define NINODE            50  // maximum number of active i-nodes
#define NDEV              10  // maximum major device number
#define ROOTDEV            1  // device number of file system root disk
#define MAXARG            32  // max exec arguments
#define MAXOPBLOCKS       10  // max # of blocks any FS op writes
#define LOGSIZE          (MAXOPBLOCKS*3)  // max data blocks in on-disk log
#define NBUF             (MAXOPBLOCKS*3)  // size of disk block cache
// #define FSSIZE           1000  // size of file system in blocks
#define FSSIZE          2000  // size of file system in blocks  // CS333 requires a larger FS.

// these two values UID/GID are used as defaults for the first process (userinit) and for setting file permissions
#define DEFAULT_UID        0  // default user id
#define DEFAULT_GID        0  // default group id
#define DEFAULT_MODE   00755  // default file permissions for user, group, and other

#define MAXPRIO            7  // maximum levels of MLFQ
#define DEFAULT_BUDGET     5  // proc budget
#define TICKS_TO_PROMOTE 100  // (1s) number of time slices until all procs are promoted in MLFQ
