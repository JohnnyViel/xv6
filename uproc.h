#define STRMAX 32

struct uproc {
  uint pid;
  uint uid;
  uint gid;
  uint ppid;
  uint prio;
  uint elapsed_ticks;
  uint cpu_ticks_total;
  char state[STRMAX];
  uint sz;
  char name[STRMAX];
};
