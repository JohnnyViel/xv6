// ps.c
// this shell command displays information of processes currently
// running on the system
#include "types.h"
#include "user.h"
#include "uproc.h"

#define MAX_PROCS 72

int
main(int argc, char*argv[])
{
  int count;
  struct uproc* uprocs = malloc(MAX_PROCS * sizeof(struct uproc));

  if((count = getprocs(MAX_PROCS,  uprocs)) < 0)
  {
    printf(2, "Could not retrieve process info!\n");
    exit();
  }

  printf(1, "PID \tName \tUID \tGID \tPPID \tPRIO \tElapsed\tCPU \tState \tSize \n"); // table header
  for (struct uproc* i = uprocs; i < &uprocs[count]; ++i)
  {
    printf(1, "%d \t%s \t%d \t%d \t%d \t%d \t%d.%d%d \t%d.%d%d \t%s \t%d \n", 
        i->pid, 
        i->name,
        i->uid,
        i->gid,
        i->ppid,
        i->prio,
        i->elapsed_ticks / 100,
        (i->elapsed_ticks % 100) / 10,
        i->elapsed_ticks % 10,
        i->cpu_ticks_total / 100,
        (i->cpu_ticks_total % 100) / 10,
        i->cpu_ticks_total % 10,
        i->state,
        i->sz);
  }
  free(uprocs);
  exit();
}
