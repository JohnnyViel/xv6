#include "types.h"
#include "user.h"

int one_arg(int (*fxn)(int), int arg, char* msg)
{
  printf(2, "%s\n", msg);
  return (*fxn)(arg);
}

int test_setget(int (*set)(uint), uint arg, uint (*get)(void), char* msg, int should_pass)
{
  set(arg);
  int ret = getuid();
  printf(2, "%s", msg);

  if (ret != arg && should_pass) {
    printf(2, "fail: %d\n", ret);
    return 0;
  }
  else {
    printf(2, "pass: %d\n", ret);
    return 1;
  }
}

int
uid_gid(void)
{
  uint pass = 0, fail = 0;
  uint uid, gid ,ppid;

  // SHOULD PASS
  printf(2, "Current UID is: %d\n", getuid());
  printf(2, "Setting UID to 100...");
  setuid(100);
  uid = getuid();
  if (uid != 100) {
    printf(2, "fail: %d\n", uid);
    ++fail;
  }
  else {
    printf(2, "pass: %d\n", uid);
    ++pass;
  }

  gid = getgid ();
  printf(2, "Current GID is: %d\n", gid);
  printf(2, "Setting GID to 100...");
  setgid(100);
  gid = getgid ();
  if (gid != 100) {
    printf(2, "fail: %d\n", gid);
    ++fail;
  }
  else {
    printf(2, "pass: %d\n", gid);
    ++pass;
  }
  
  printf(2, "Getting PPID...");
  ppid = getppid ();
  if (ppid < 0) {
    printf(2, "fail: %d\n", ppid);
    ++fail;
  }
  else {
    printf(2, "pass: %d\n", ppid);
    ++pass;
  }

  // SHOULD FAIL
  printf(2, "Should-fail commands:\n");
  printf(2, "Setting GID to 99999...");
  if (setgid(99999) == 0) {
    printf(2, "fail: %d\n", gid);
    ++fail;
  }
  else {
    printf(2, "pass: %d\n", gid);
    ++pass;
  }
  printf(2, "Setting GID to -1...");
  if (setgid(-1) == 0) {
    printf(2, "fail: %d\n", gid);
    ++fail;
  }
  else {
    printf(2, "pass: %d\n", gid);
    ++pass;
  }
  printf(2, "Setting UID to 99999...");
  if (setuid(99999) == 0) {
    printf(2, "fail: %d\n", uid);
    ++fail;
  }
  else {
    printf(2, "pass: %d\n", uid);
    ++pass;
  }
  printf(2, "Setting UID to -1...");
  if (setuid(-1) == 0) {
    printf(2, "fail: %d\n", uid);
    ++fail;
  }
  else {
    printf(2, "pass: %d\n", uid);
    ++pass;
  }

  printf(2, "total tests: %d\n", pass + fail);
  printf(2, "passed: %d\n", pass);
  printf(2, "failed: %d\n", fail);
  return (pass + fail);
}

int
main(int argc, char* argv[])
{
  int ntests = 0;
  int ticks  = uptime();

  ntests += uid_gid();

  ticks = uptime() - ticks;
  printf(2, "\n%d tests ran in %d.%d%d s\n",
            ntests,
            ticks / 100,
            (ticks % 100) / 10,
            ticks % 10);
            
  exit();
}

