// chown.c
// This program takes an integer greater than or equal to 0 and, if the user has write permission on the file and the new
// number is valid, sets the owner of the file to the new ID
#include "types.h"
#include "user.h"

#ifdef CS333_P5
int
main(int argc, char *argv[])
{
  if (argc != 3){
    printf(2, "usage: %s expects 2 arguments\n", argv[0]);
    exit();
  }

  if (argv[1][0] == '-'){
    printf(2, "owner ID must be an integer from 0 - 32767\n");
    exit();
  }
  
  int owner = atoi(argv[1]);
  if (owner < 0 || owner > 32767){
    printf(2, "owner ID must be an integer from 0 - 32767\n");
    exit();
  }

  if (chown(argv[2], owner))
    printf(2, "chown failed\n");
  exit();
}

#else
int main(){
  printf(2, "chown not implemented without CS333_P5 flag defined in makefile\n");
  exit();
}
#endif
