// chgrp.c
// This program takes an integer greater than or equal to 0 and, if the 
// number is valid, sets the group of the file to the new group ID
#include "types.h"
#include "user.h"

#ifdef CS333_P5
int
main(int argc, char *argv[])
{
  if (argc != 3){
    printf(2, "usage: %s expects 2 arguments\n", argv[0]);
    exit();
  }

  if (argv[1][0] == '-'){
    printf(2, "group ID must be an integer from 0 - 32767\n");
    exit();
  }

  int group = atoi(argv[1]);
  if (group < 0 || group > 32767){
    printf(2, "group ID must be an integer from 0 - 32767\n");
    exit();
  }

  if (chgrp(argv[2], group))
    printf(2, "%s failed\n", argv[0]);
  exit();
}

#else
int main(){
  printf(2, "chgrp not implemented without CS333_P5 flag defined in makefile\n");
  exit();
}
#endif
